import socket

target_host = "10.30.55.37"
target_port = 191  

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect((target_host, target_port))    

file_contents = ""
while True:
    data = client.recv(1024).decode()
    if not data:
        break
    file_contents += data

client.close()

print("Received file contents:")
print(file_contents)
