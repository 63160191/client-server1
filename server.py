import socket

with open("a.txt", "r") as file:  
    file_contents = file.read()

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(("", 191))
server.listen(1)

print("Server is listening for connections...")

client_socket, client_address = server.accept()

print(f"Accepted connection from: {client_address}")

client_socket.sendall(file_contents.encode())

client_socket.close() 
server.close()
